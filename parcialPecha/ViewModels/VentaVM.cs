﻿using parcialPecha.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace parcialPecha.ViewModels
{
    public class VentaVM
    {
        public Venta ventaModel { get; set; }
        public List<FormaPago> formaPago { get; set; }
        public List<PlanesCuota> planesCuota { get; set; }
    }
}