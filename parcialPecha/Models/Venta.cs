﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace parcialPecha.Models
{
    public class Venta
    {
        public int id { get; set; }
        [Required(ErrorMessage = "El nombre del cliente es requerido")]
        public string nombreCliente { get; set; }

        [Required(ErrorMessage = "La forma de pago es requerida")]
        public int IdFormaPago { get; set; }

        [Required(ErrorMessage = "El plan de cuotas es requerido")]
        public int IdPlanesCuotas { get; set; }

        [Required(ErrorMessage = "El importe es requerido")]
        public double importe { get; set; }
    }
}