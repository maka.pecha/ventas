﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace parcialPecha.Models
{
    public class FormaPago
    {
        [Required(ErrorMessage = "La forma de pago es requerida")]
        public int id { get; set; }
        public string nombre { get; set; }
        public float porcentajeInteres { get; set; }
    }
}