﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace parcialPecha.DataTransferObjects
{
    public class VentaDTO
    {
        public int id { get; set; }
        public string nombreCliente { get; set; }

        public string formaPago { get; set; }

        public string planesCuotas { get; set; }

        public double importe { get; set; }
    }
}