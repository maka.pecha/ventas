﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace parcialPecha.DataTransferObjects
{
    public class CantidadVentasPorFormaPagoDTO
    {
        public int id { get; set; }
        public string nombre { get; set; }
        public int cantidad { get; set; }
    }
}