﻿using parcialPecha.AccesoDatos;
using parcialPecha.DataTransferObjects;
using parcialPecha.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace parcialPecha.Controllers
{
    public class VentaController : Controller
    {
        // GET: Persona
        public ActionResult AltaVenta()
        {
            VentaVM vm = new VentaVM();
            vm.formaPago = AD_Ventas.ObtenerListaFormaPago();
            vm.planesCuota = AD_Ventas.ObtenerListaPlanesCuotas();

            return View(vm);
        }
        // para hacer el post de la nueva persona y ver la lista
        [HttpPost]
        public ActionResult AltaVenta(VentaVM model)
        {
            if (ModelState.IsValid)
            {
                bool resultado = AD_Ventas.InsertarNuevaVenta(model.ventaModel);
                if (resultado)
                {
                    return RedirectToAction("ListadoVentas", "Venta");
                }
                else
                {
                    return View(model);
                }
            }
            else
            {
                //cuando la Vista vuelve a cargar el DropDownListFor()
                //carga los id de formaPago y planesCuota pero no los values, 
                //para eso hay que volver a mandarle ambas listas
                model.formaPago = AD_Ventas.ObtenerListaFormaPago();
                model.planesCuota = AD_Ventas.ObtenerListaPlanesCuotas();
                return View(model);
            }
        }

        // get de la lista
        public ActionResult ListadoVentas()
        {
            List<VentaDTO> lista = AD_Ventas.ObtenerListaVentas();
            return View(lista);
        }

        public ActionResult Reportes()
        {
            List<CantidadVentasPorFormaPagoDTO> lista = AD_Ventas.ObtenerListaCantidadVentasPorFormaPago();

            ReportesVM vm = new ReportesVM();
            vm.cantidadVentasPorFormaPago = lista;

            return View(vm);
        }


    }
}