﻿using parcialPecha.DataTransferObjects;
using parcialPecha.Models;
using parcialPecha.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace parcialPecha.AccesoDatos
{
    public class AD_Ventas
    {
        public static bool InsertarNuevaVenta(Venta ven)
        {
            bool resultado = false;

            string cadenaConexion = System.Configuration.ConfigurationManager.AppSettings["CadenaBD"].ToString();

            SqlConnection cn = new SqlConnection(cadenaConexion);

            try
            {
                SqlCommand cmd = new SqlCommand();

                string consulta = "insert into Ventas values(@NombreCliente, @IdFormaPago, @IdPlanesCuota, @Importe)";
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("@NombreCliente", ven.nombreCliente); //asigno parametros al command
                cmd.Parameters.AddWithValue("@IdFormaPago", ven.IdFormaPago);
                cmd.Parameters.AddWithValue("@IdPlanesCuota", ven.IdPlanesCuotas);
                cmd.Parameters.AddWithValue("@Importe", ven.importe);

                cmd.CommandType = System.Data.CommandType.Text; //porque yo le mando la sentencia
                cmd.CommandText = consulta; //esta es la sentencia

                //la consulta
                cn.Open(); //abro la conexion
                cmd.Connection = cn; //le indico la conexion a la que se va a conectar y ya está abierta
                cmd.ExecuteNonQuery(); //ejecuta la sentencia
                resultado = true; // si no hubo error es true
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                cn.Close();//si hay o no error igual se cierra la conexion
            }

            return resultado;
        }

        public static List<FormaPago> ObtenerListaFormaPago()
        {
            List<FormaPago> resultado = new List<FormaPago>();

            string cadenaConexion = System.Configuration.ConfigurationManager.AppSettings["CadenaBD"].ToString();

            SqlConnection cn = new SqlConnection(cadenaConexion);

            try
            {
                SqlCommand cmd = new SqlCommand();

                string consulta = "select * from FormaPago";
                cmd.Parameters.Clear();

                cmd.CommandType = System.Data.CommandType.Text; //porque yo le mando la sentencia
                cmd.CommandText = consulta; //esta es la sentencia

                //la consulta
                cn.Open(); //abro la conexion
                cmd.Connection = cn; //le indico la conexion a la que se va a conectar y ya está abierta

                SqlDataReader dr = cmd.ExecuteReader(); // se instancia con el resultado de la operacion

                if (dr != null)
                {
                    while (dr.Read())
                    {
                        FormaPago aux = new FormaPago();
                        aux.id = int.Parse(dr["Id"].ToString());
                        aux.nombre = dr["Nombre"].ToString();
                        aux.porcentajeInteres = float.Parse(dr["PorcentajeInteres"].ToString());

                        resultado.Add(aux);
                    }
                }


            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                cn.Close();//si hay o no error igual se cierra la conexion
            }

            return resultado;
        }

        public static List<PlanesCuota> ObtenerListaPlanesCuotas()
        {
            List<PlanesCuota> resultado = new List<PlanesCuota>();

            string cadenaConexion = System.Configuration.ConfigurationManager.AppSettings["CadenaBD"].ToString();

            SqlConnection cn = new SqlConnection(cadenaConexion);

            try
            {
                SqlCommand cmd = new SqlCommand();

                string consulta = "select * from PlanesCuotas";
                cmd.Parameters.Clear();

                cmd.CommandType = System.Data.CommandType.Text; //porque yo le mando la sentencia
                cmd.CommandText = consulta; //esta es la sentencia

                //la consulta
                cn.Open(); //abro la conexion
                cmd.Connection = cn; //le indico la conexion a la que se va a conectar y ya está abierta

                SqlDataReader dr = cmd.ExecuteReader(); // se instancia con el resultado de la operacion

                if (dr != null)
                {
                    while (dr.Read())
                    {
                        PlanesCuota aux = new PlanesCuota();
                        aux.id = int.Parse(dr["Id"].ToString());
                        aux.cantidadCuotas = int.Parse(dr["CantidadCuotas"].ToString());
                        aux.porcentajeInteres = float.Parse(dr["PorcentajeInteres"].ToString());

                        resultado.Add(aux);
                    }
                }


            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                cn.Close();//si hay o no error igual se cierra la conexion
            }

            return resultado;
        }

        public static List<VentaDTO> ObtenerListaVentas()
        {
            List<VentaDTO> resultado = new List<VentaDTO>();

            string cadenaConexion = System.Configuration.ConfigurationManager.AppSettings["CadenaBD"].ToString();

            SqlConnection cn = new SqlConnection(cadenaConexion);

            try
            {
                SqlCommand cmd = new SqlCommand();

                string consulta = @"select v.Id, v.NombreCliente, fp.Nombre as FormaPago, pc.CantidadCuotas as PlanesCuotas, (v.Importe+ (v.Importe*pc.PorcentajeInteres/100) + (v.Importe*fp.PorcentajeInteres/100)) as Importe  
                                    from Ventas v
                                    join PlanesCuotas pc on pc.Id = v.IdPlanesCuota
									join FormaPago fp on fp.Id = v.IdFormaPago";
                cmd.Parameters.Clear();

                cmd.CommandType = System.Data.CommandType.Text; //porque yo le mando la sentencia
                cmd.CommandText = consulta; //esta es la sentencia

                //la consulta
                cn.Open(); //abro la conexion
                cmd.Connection = cn; //le indico la conexion a la que se va a conectar y ya está abierta

                SqlDataReader dr = cmd.ExecuteReader(); // se instancia con el resultado de la operacion

                if (dr != null)
                {
                    while (dr.Read())
                    {
                        VentaDTO aux = new VentaDTO();
                        aux.id = dr.GetInt32(0);
                        aux.nombreCliente = dr.GetString(1);
                        aux.formaPago = dr["FormaPago"].ToString();
                        aux.planesCuotas = dr["PlanesCuotas"].ToString();
                        aux.importe = float.Parse(dr["Importe"].ToString());

                        resultado.Add(aux);
                    }
                }
                dr.Close();

            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                cn.Close();//si hay o no error igual se cierra la conexion
            }

            return resultado;
        }

        public static List<CantidadVentasPorFormaPagoDTO> ObtenerListaCantidadVentasPorFormaPago()
        {
            List<CantidadVentasPorFormaPagoDTO> resultado = new List<CantidadVentasPorFormaPagoDTO>();

            string cadenaConexion = System.Configuration.ConfigurationManager.AppSettings["CadenaBD"].ToString();

            SqlConnection cn = new SqlConnection(cadenaConexion);

            try
            {
                SqlCommand cmd = new SqlCommand();

                string consulta = @"select fp.Id, fp.Nombre, count(*) as Cantidad
                                    from Ventas v
                                    join FormaPago fp on fp.Id = v.IdFormaPago
                                    group by fp.Nombre, fp.Id;";
                cmd.Parameters.Clear();

                cmd.CommandType = System.Data.CommandType.Text; //porque yo le mando la sentencia
                cmd.CommandText = consulta; //esta es la sentencia

                //la consulta
                cn.Open(); //abro la conexion
                cmd.Connection = cn; //le indico la conexion a la que se va a conectar y ya está abierta

                SqlDataReader dr = cmd.ExecuteReader(); // se instancia con el resultado de la operacion

                if (dr != null)
                {
                    while (dr.Read())
                    {
                        CantidadVentasPorFormaPagoDTO aux = new CantidadVentasPorFormaPagoDTO();
                        aux.id = int.Parse(dr["Id"].ToString());
                        aux.nombre = dr["Nombre"].ToString();
                        aux.cantidad = int.Parse(dr["Cantidad"].ToString());

                        resultado.Add(aux);
                    }
                }


            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                cn.Close();//si hay o no error igual se cierra la conexion
            }

            return resultado;
        }

    }
}